using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.InputSystem.XR;

public class NetworkRef : MonoBehaviour
{
    public PhotonView PV;
    public SquatCounter SC;
    public GameObject Button;
    public GameObject[] hands;
    
    public Transform fake;
    // Start is called before the first frame update
    void Start()
    {
        if (PV.IsMine)
        {
            this.GetComponent<Camera>().enabled = true;
            this.GetComponent<TrackedPoseDriver>().enabled = true;
            this.GetComponent<AudioListener>().enabled = true;
            Button.SetActive(true);
            
            #if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                this.SC.camera = fake;
                fake.gameObject.SetActive(true);
                hands[0].SetActive(false);
                hands[1].SetActive(false);
            #endif
        }
        else
        {
            #if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                        hands[0].SetActive(false);
                        hands[1].SetActive(false);
            #endif
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
