using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using Photon;
using Photon.Realtime;
using Random = UnityEngine.Random;


public class SquatCounter : MonoBehaviourPunCallbacks
{
    
    public Renderer render;
    public PhotonView PV;
    public Transform camera;
    private Vector3 origin;
    public float SquatHeight;
    public TextMeshPro Txt;
    public GameObject Button;
    public Animation anim;
    private int counter;
    private bool OnSerie;

    
    private Color color;
    private bool Squated = false;
    // Start is called before the first frame update
    void Start()
    {
        this.color = new Color( Random.Range(0f,1f),Random.Range(0f,1f),Random.Range(0f,1f) );
        origin = camera.position;
        Txt.gameObject.SetActive(true);
        OnSerie = false;
        PV.RPC("UpdateColor",RpcTarget.AllBufferedViaServer,this.color.r,this.color.g,this.color.b);

        for (int j = 0; j < 1; j++)
        {
            PV.RPC("updatePos",RpcTarget.AllBufferedViaServer,0,false);    
        }
        
        
    }
    
    [PunRPC]
    private void updatePos(int idx,bool occuped)
    {
       
    }
    


    private float i = 0;
    
    // Update is called once per frame
    void Update()
    {

        float max = 1.5f;
        float min = 1f;

        float value = Mathf.Clamp(camera.transform.localPosition.y, min, max);
        anim["squat"].normalizedTime = Mathf.Clamp01( Mathf.InverseLerp(1.5f,1f,value));
        Txt.text = counter.ToString();
        
        if (!PV.IsMine)
        {
            return;
        }
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            origin = camera.localPosition;
            counter = 0;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartSerie();
        }
        
        
        if (Input.GetKey(KeyCode.A))
        {
            camera.transform.localPosition = new Vector3(camera.transform.localPosition.x,camera.transform.localPosition.y + Time.deltaTime*1f,camera.localPosition.z);
        }
        
        if (Input.GetKey(KeyCode.E))
        {
            camera.transform.localPosition = new Vector3(camera.transform.localPosition.x,camera.transform.localPosition.y - Time.deltaTime*1f,camera.localPosition.z);
        }
        
        if (Math.Abs((camera.localPosition.y - origin.y)) > SquatHeight && !Squated)
        {
            Squated = true;
       
        }
        else if(Math.Abs((camera.localPosition.y - origin.y)) <= 0.2f && Squated)
        {
            this.counter++;
            PV.RPC("UpdateCounter",RpcTarget.AllBufferedViaServer,this.counter);
           
            
            
            Squated = false;
        }

   
    }

    [PunRPC]
    protected virtual void UpdateCounter(int counter)
    {
        this.counter = counter;
    }

    [PunRPC]
    private void UpdateColor(float colora, float colorb,float colorc)
    {
        this.render.material.color = new Color(colora,colorb,colorc,0f);
    }
    
    public void StartSerie()
    {
        origin = camera.localPosition;
        counter = 0;
     
        OnSerie = true;
        Button.SetActive(false);
    }
}
