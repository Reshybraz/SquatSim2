using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Round : MonoBehaviour
{

    public float speed;
    public float radius;
    private float c = 0;
    
    // Update is called once per frame
    void Update()
    {

        c = (c +1*Time.deltaTime*speed)%360;

        Vector3 pos = this.transform.position;
        
        this.transform.position = new Vector3(Mathf.Sin(c)*radius,this.transform.position.y, Mathf.Cos(c)*radius);

        Vector3 dir = pos - this.transform.position;

        this.transform.rotation = Quaternion.LookRotation(dir);

    }
}
